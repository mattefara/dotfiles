#!/bin/bash
# changeBrightness

# Arbitrary but unique message tag
msgTag="brightness-controller"

# Change the brightness using xbacklight
xbacklight "$@" > /dev/null

brightness="$(xbacklight -get)"
dunstify -a $msgTag -u low -i audio-volume-high -h string:x-dunst-stack-tag:$msgTag \
    -h int:value:"$brightness" "Brightness: ${brightness}%"

