#!/usr/bin/env bash

uptime=$(uptime -p | sed -e 's/up //g')

# Options
shutdown=" Shutdown"
reboot=" Restart"
lock=" Lock"
hibernate=" Hibernate"
logout=" Logout"

# Variable passed to rofi
options="$lock\n$hibernate\n$logout\n$reboot\n$shutdown"

chosen="$(echo -e "$options" | rofi -i -p "Uptime: $uptime" -dmenu -selected-row 0)"
case $chosen in
    $shutdown)
		systemctl poweroff
        ;;
    $reboot)
		systemctl reboot
        ;;
    $lock)
        if [ -x "$(command -v betterlockscreen)" ]; then
			betterlockscreen -l
		fi
        ;;
    $hibernate)
		amixer set Master mute
		systemctl hibernate
        ;;
    $logout)
        if [ -x "$(command -v loginctl)" ]; then
			loginctl kill-session $XDG_SESSION_ID
        fi
        ;;
esac
